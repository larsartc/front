import ResultsCheckModal from './ResultsCheckModal';
import CrawlerResultsModal from './CrawlerResultsModal';

export {
    ResultsCheckModal,
    CrawlerResultsModal
}