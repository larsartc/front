import React, { useState, useEffect } from 'react';

import { Modal, Button } from 'react-bootstrap';

interface StatusCheckModalProps {
    show: boolean;
    handleVisibility: React.Dispatch<React.SetStateAction<boolean>>;
}

const StatusCheckModal: React.FC<StatusCheckModalProps> = ({show, handleVisibility}) => {

  const [isVisible, setIsVisible] = useState(false);

  useEffect(() => {
      setIsVisible(show);
  }, [show]);

  const handleClose = () => {
    setIsVisible(false);
    handleVisibility(false);
  }

  return (
    <Modal show={isVisible} onHide={handleClose}>
        <Modal.Header closeButton>
            <Modal.Title>Status Check</Modal.Title>
        </Modal.Header>
        <Modal.Body>Crawler X still runnning!</Modal.Body>
        <Modal.Footer>
            <Button variant="secondary" onClick={handleClose}>Close</Button>
        </Modal.Footer>
    </Modal>
  );
}

export default StatusCheckModal;