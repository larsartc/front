import React from 'react';

import { BrowserRouter, Route } from 'react-router-dom';

import Login from './views/Login';
import List from './views/List'; 

function App() {
  return (
    <BrowserRouter>
      <Route path="/" component={Login} exact />
      <Route path="/list" component={List} />
    </BrowserRouter>
  );
}

export default App;
