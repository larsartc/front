import React, { useState } from 'react';
import { useHistory } from 'react-router-dom';

import { Base64 } from 'js-base64';

import { 
    Container, 
    Row, 
    Card,
    Form,
    Button,
    Toast
} from 'react-bootstrap';

import api from '../services/api';

interface AuthData {
    email: string;
    password: string;
}

const Login: React.FC = () => {
  const history = useHistory();
  const [isAuthenticating, setIsAuthenticating] = useState(false);
  const [authData, setAuthData] = useState<AuthData>({
      email: "",
      password: ""
  });
  const [showToast, setShowToast] = useState(false);
  const [toastText, setToastText] = useState("");
  
  
  function handleToast(text: string) {
    setShowToast(true);
    setToastText(text);
  }

  async function handleLogin(event: any) {
    event.preventDefault();
    setIsAuthenticating(true);
    try {
        await api.post('/auth', {}, {
            headers: {
                authorization: "Basic " + Base64.encode(`${authData.email}:${authData.password}`)
            }
        });

        history.push("/list");
    }  catch(e) {
        handleToast("Invalid username or password, please check if it's correct!");
        console.log(e);
    }
    setIsAuthenticating(false);
  }

  function handleChange(event: any) {
      const value = event.target.value.toString().trim()
      if(value && value.length > 1) {
        setAuthData({
            ...authData,
            [event.target.name]: value
        });
      }
  }

  return (
    <>
        <Toast 
            onClose={() => setShowToast(false)} 
            show={showToast} 
            delay={3000} 
            autohide
            style={{
                position: 'absolute',
                top: 0,
                right: 0,
            }}
        >
            <Toast.Header>
                <span></span>
            </Toast.Header>
            <Toast.Body>{toastText}</Toast.Body>
        </Toast>
        <Container fluid style={{ display: "flex", alignItems: "center", justifyContent: "center" }}>
            <Row>
                <Card style={{ width: '28rem' }}>
                    <Card.Body>
                        <Card.Title>Sign In</Card.Title>
                        <Form onSubmit={handleLogin}>
                            <Form.Group controlId="formBasicEmail">
                                <Form.Label>Email</Form.Label>
                                <Form.Control type="email" placeholder="Your Email" name="email" onChange={handleChange} />
                            </Form.Group>

                            <Form.Group controlId="formBasicPassword">
                                <Form.Label>Password</Form.Label>
                                <Form.Control type="password" placeholder="Your Password" name="password" onChange={handleChange} />
                            </Form.Group>
                            
                            <Button variant="primary" type="submit">
                                {isAuthenticating ? "Authenticating ..." : "Sign In"}
                            </Button>
                        </Form>
                    </Card.Body>
                </Card>
            </Row>
        </Container>
    </>
  );
}

export default Login;