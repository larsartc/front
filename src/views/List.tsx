import React, { useState, useEffect } from 'react';

import { 
    Container,
    Row,
    Table,
    Button,
    OverlayTrigger,
    Tooltip,
    Badge
 } from 'react-bootstrap';

import { FiStopCircle, FiPlayCircle, FiCheck, FiBarChart2 } from 'react-icons/fi';

import { ResultsCheckModal, CrawlerResultsModal } from '../components/modals';
import api from '../services/api';

interface Crawler {
    id: string;
    name: string;
    status: string;
}

const List: React.FC = () => {

  const [isRunning, setIsRunning] = useState(false);
  const [resultsCheckModal, setResultsCheckModal] = useState(false);
  const [crawlerResultsModal, setCrawlerResultsModal] = useState(false);
  const [crawlers, setCrawlers] = useState([]);

  const handleShowResultsCheckModal = () => setResultsCheckModal(true);
  const handleShowCrawlerResultsModal = () => setCrawlerResultsModal(true);

  useEffect(() => {
    async function getCrawlers() {
        const { data } = await api.get('/crawlers');
        setCrawlers(data);
    }

    getCrawlers();
  }, []);

  function statusBadge(status: string) {
    switch(status) {
        case "idle": return <Badge variant="warning">Idle</Badge>;
        case "running": return <Badge variant="success">Running</Badge>;
        case "stopped": return <Badge variant="danger">Stopped</Badge>;
        case "failed": return <Badge variant="danger">Failed</Badge>;
        case "finished": return <Badge variant="info">Finished</Badge>;
    }
  }

  async function start(id: string) {
    setIsRunning(!isRunning);
    await api.post('/crawlers/');
    
  }

  return (
      <>
        <Container>
            <Row>
                <Table>
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Crawler</th>
                            <th>Status</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        {crawlers && crawlers.map((crawler: Crawler, i) => {
                            return (
                                <tr>
                                    <td>{i + 1}</td>
                                    <td>{crawler.name}</td>
                                    <td>
                                        {statusBadge(isRunning ? crawler.status : "running")}
                                    </td>
                                    <td>
                                        <OverlayTrigger
                                            key="top"
                                            placement="top"
                                            overlay={
                                                <Tooltip id="playstop">
                                                    {isRunning ? "Play" : "Stop"}
                                                </Tooltip>
                                            }
                                        >
                                            <Button 
                                                variant={isRunning ? "danger" : "success"} 
                                                onClick={() => {start(crawler.id)}}
                                            >
                                                {isRunning ? <FiStopCircle /> : <FiPlayCircle />}
                                            </Button>
                                        </OverlayTrigger>
        
                                        <OverlayTrigger
                                            key="top"
                                            placement="top"
                                            overlay={
                                                <Tooltip id="resultcheck">
                                                    Results Check
                                                </Tooltip>
                                            }
                                        >
                                            <Button variant="primary" onClick={handleShowResultsCheckModal}><FiCheck /></Button>
                                        </OverlayTrigger>
        
                                        <OverlayTrigger
                                            key="top"
                                            placement="top"
                                            overlay={
                                                <Tooltip id="resultcheck">
                                                    Crawler Results
                                                </Tooltip>
                                            }
                                        >
                                            <Button variant="info" onClick={handleShowCrawlerResultsModal}><FiBarChart2 /></Button>
                                        </OverlayTrigger>
                                    </td>
                                </tr>
                            );
                        })}
                    </tbody>
                </Table>
            </Row>
        </Container>

        <ResultsCheckModal show={resultsCheckModal} handleVisibility={setResultsCheckModal} />
        <CrawlerResultsModal show={crawlerResultsModal} handleVisibility={setCrawlerResultsModal} />
      </>
  );
}

export default List;